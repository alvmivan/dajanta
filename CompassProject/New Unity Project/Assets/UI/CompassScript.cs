﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompassScript : MonoBehaviour
{
    
    [SerializeField] private RawImage CompassImage;
    [SerializeField] private Transform CameraTransform;
    private float _Offset;
    
    private void Start()
    {
        Input.compass.enabled = true;
        _Offset = Input.compass.trueHeading;
    }

    void Update ()
    {
        float xDir = ((-1 * CameraTransform.localEulerAngles.y) + _Offset)/ 360;
        CompassImage.uvRect = new Rect(xDir, 0, 1, 1);
    }
}
