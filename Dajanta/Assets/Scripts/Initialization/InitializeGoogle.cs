using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Initialization
{
    public class InitializeGoogle : MonoBehaviour
    {
        private static bool _isInitialization = true;
        private void Start()
        {
            if (!_isInitialization) return;
            _isInitialization = false;
            var config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.Activate();
            
            var user = Social.localUser;
            user.Authenticate(success=>{});
        }
    }
    
    
    
}