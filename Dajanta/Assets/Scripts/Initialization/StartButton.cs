﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Initialization
{
    public class StartButton : MonoBehaviour , IPointerClickHandler
    {
        [SerializeField] private string sceneGamePlay = "GamePlay";

        [SerializeField] private Slider musicVol;
        [SerializeField] private Slider sfxVol;

        private void Update()
        {
            StaticSettings.VolMusic = musicVol.value;
            StaticSettings.VolSxf = sfxVol.value;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            
            SceneManager.LoadScene(sceneGamePlay);
        }
    }
}
