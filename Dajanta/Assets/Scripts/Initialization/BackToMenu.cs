﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Initialization
{
    public class BackToMenu : MonoBehaviour
    {
        [SerializeField] private string MenuSceneName = "Intro";
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(MenuSceneName);
            }
        }
    }
}
