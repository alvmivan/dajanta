﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;
using Image = UnityEngine.UI.Image;

namespace Initialization
{
    public class OptionsButton : MonoBehaviour , IPointerClickHandler 
    {
        [SerializeField] private Image optionsPanel;
        [SerializeField] private TMP_Text MyText;
        private bool active = false;
        
        private void OnValidate()
        {
            optionsPanel.gameObject.SetActive(active);
            if (!MyText)
            {
                MyText = GetComponent<TMP_Text>();
            }
            ProcessAlpha();
        }

        private void ProcessAlpha()
        {
            var color = MyText.color;
            color.a = active ? 1 : 0.5f;
            MyText.color = color;
        }
        

        public void OnPointerClick(PointerEventData eventData)
        {
            active = !active;
            print(active);
            optionsPanel.gameObject.SetActive(active);
            ProcessAlpha();
        }
    }
}
