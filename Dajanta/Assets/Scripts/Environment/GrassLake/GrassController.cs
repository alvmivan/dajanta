﻿using UnityEngine;

namespace Environment.GrassLake
{
    public class GrassController : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private LayerMask duckmask;
        [SerializeField] private Vector3 RelativeMed;
        [SerializeField] private float CircleRatio;
        
        // ----------- Animator  Constants ---------------
        
        private static readonly int Hit = Animator.StringToHash("Hit");
        private static readonly int RandomValue = Animator.StringToHash("RandomValue");
        private static readonly int Rotate = Animator.StringToHash("Rotate");
        private const float ChanceRotateAnimator = .1f;            

        // ---------- Variables -------------
        
        private bool _grassDown = false;
        
        // --------------- Unity Callbacks -------------
        
        private void Update()
        {
            if (_grassDown) return;
            
            animator.SetFloat(RandomValue,Random.value);
            
            if (CheckDuckContact())
            {
                _grassDown = true;
                animator.SetTrigger(Hit);
            }
            else
            {
                if(Random.value<ChanceRotateAnimator)
                animator.SetTrigger(Rotate);
            }
        }

        private void OnDrawGizmosSelected()
        {
            var absoluteMed = transform.position + RelativeMed;
            Gizmos.color =Color.green;
            Gizmos.DrawWireSphere(absoluteMed,CircleRatio);
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(absoluteMed,Vector3.one * CircleRatio*.05f);
        }

        private void Start()
        {
            transform.localScale *= Random.Range(.75f, 1.5f);
            transform.Rotate(0,Random.Range(-360,360),0);
        }

        // --------------- Private methods --------------
        
        private bool CheckDuckContact()
        {
            return Physics.CheckSphere(transform.position + RelativeMed, CircleRatio, duckmask);
        }
        
        // --------------- Callbacks --------------
                    
        
        private void _grass_up() // <-- called by event
        {
            _grassDown = false;
        }
        
        
    }
}
