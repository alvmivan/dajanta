﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Achievements
{
    public class ShowLeaderBoard : MonoBehaviour , IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            Scores.ShowLeaderboardsUI();
            transform.position -= Vector3.up * 10;
        }
    }
}
