using GooglePlayGames;
using GooglePlayGames.Native.Cwrapper;
using UnityEngine;

namespace Achievements
{
    
    
    
    
    public static class Scores
    {
        public static IAchievementsHandler GetHandler() => new AchievementsHandler();
        public static void IncAchievement(string id, int stepsToIncrement)
        {
            if (id == "") return;
            PlayGamesPlatform.Instance.IncrementAchievement(id, stepsToIncrement, success => { });
        }
        public static void ReportProggress(string id, int value)
        {
            if (id == "") return;
            Social.ReportProgress(id,value,success=>{});
        }
        public static int GetAchievement(string id)
        {
            if (id == "") return 0;
            return (int) PlayGamesPlatform.Instance.GetAchievement(id).Points;
        }
        public static void AddScore(string leaderboardId, long score)
        {
            if (leaderboardId == "") return;
            Social.ReportScore(score, leaderboardId, success => { });
        }
        
        #region UIs
        public static void ShowAchievementsUI()
        {
            Social.ShowAchievementsUI();
        }
        
        
        
        public static void ShowLeaderboardsUI()
        {
            Social.ShowLeaderboardUI();
        }
        #endregion

        
    }

    public interface IAchievementsHandler
    {
        void ShootMiss();
        void ShootOk(int money);
    }

    internal class AchievementsHandler : IAchievementsHandler
    {
        
        private int _consecutiveMiss = 0;
        private int _consecutiveShoot = 0;
        
        public void ShootMiss()
        {
            _consecutiveShoot = 0;
            _consecutiveMiss++;
            /*
            foreach (var key in _achievementsFail)
            {
                Scores.ReportProggress(key,_consecutiveMiss);
            }*/
        }

        public void ShootOk(int money)
        {
            _consecutiveShoot++;
            _consecutiveMiss = 0;
            var akey = "";
            foreach (var key in _achievementsDucks)
            {
                akey = key;
                Scores.IncAchievement(key,1);
            }
            
            /*var ducks = Scores.GetAchievement(akey);
            Scores.AddScore(akey,ducks+1);
            */
            foreach (var key in _achievementsMoney)
            {
                akey = key;
                Scores.IncAchievement(key,money);
            }
            
/*            var m = Scores.GetAchievement(akey);
            Scores.AddScore(akey,m+1);
*/
            foreach (var key in _consecutives)
            {
                Scores.ReportProggress(key,_consecutiveShoot);
            }
        }



        private readonly string[] _consecutives = new[] {"CgkIqL_B1ukTEAIQAA","CgkIqL_B1ukTEAIQAg"};
        private readonly string[] _achievementsDucks = new[] {"CgkIqL_B1ukTEAIQAQ"};
        private readonly string[] _achievementsMoney = new[] {"CgkIqL_B1ukTEAIQAw"};
        private readonly string[] _achievementsFail = new[] {""};

        private readonly string _leaderBoardMoney = "CgkIqL_B1ukTEAIQBA";
        private readonly string _leaderBoardDucks = "CgkIqL_B1ukTEAIQBQ";


    }
    
    
    
    
    
    
}