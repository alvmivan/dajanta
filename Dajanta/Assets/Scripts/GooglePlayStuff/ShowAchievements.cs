﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Achievements
{
    public class ShowAchievements : MonoBehaviour , IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            Scores.ShowAchievementsUI();
            transform.position -= Vector3.up * 10;
        }
    }
}

