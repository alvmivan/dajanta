using Sound.Standalone;

namespace Sound
{
    public interface ISoundSystem
    {
        void Shoot();
    }

    public static class SoundManager
    {
        public static ISoundSystem SoundSystem { get; } = new SoundSystem();
    }

    public class SoundSystem : ISoundSystem
    {
        public void Shoot()
        {
            var source = SoundBuckets.Instance.ShootSound;
            source.Play();
        }
    }
}