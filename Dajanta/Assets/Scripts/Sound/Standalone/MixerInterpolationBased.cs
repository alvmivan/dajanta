using System.Collections.Generic;
using Initialization;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Sound.Standalone
{
    internal class MixerInterpolationBased : LoopsManager
    {
        // --------- Variables -------------

        private readonly ICollection<AudioSource> _changing = new HashSet<AudioSource>();
        private readonly ICollection<AudioSource> _volUp = new HashSet<AudioSource>();
        

        // todo: {tip for next refactor} :  change the 3 dictionary <Audio->float> for a Dict <Audio->3floatsStructure>
        //.... Vector3 ? .... or less weird :P

        // --------- unity Callbacks --------------

        protected override void Validations()
        {
            _changing.Clear();
            _volUp.Clear();
        }

        // todo: {tip for next refactor} : throws common stuff in both mixers to loops manager
        private void Start()
        {
            foreach (var audioSource in loops)
            {
                audioSource.Play();
                if (!untouchables.Contains(audioSource))
                {
                    _changing.Add(audioSource);
                }

                audioSource.volume = Random.value * Vol;
            }
            

            foreach (var source in startsWithFadeIn)
            {
                source.volume = Random.value * Random.value * Random.value;
                _volUp.Add(source);
            }
        }

        

        private void FixedUpdate()
        {
            Vol = Mathf.Clamp(Vol,0, StaticSettings.VolMusic);
            if (Vol <= 0.00001f)
            {
                foreach (var audioSource in _changing)
                {
                    audioSource.volume = 0;
                }
                foreach (var audioSource in _volUp)
                {
                    audioSource.volume = 0;
                }
                return;
            }
            foreach (var source in _changing)
            {
                if (_volUp.Contains(source))
                {
                    if (source.volume < Vol)
                    {
                        source.volume += .1f *Random.value *Random.value * Time.fixedDeltaTime;
                    }
                    else
                    {
                        _volUp.Remove(source);
                    }
                }
                else
                {
                    if (source.volume > 0)
                    {
                        source.volume -= .1f *Random.value *Random.value * Time.fixedDeltaTime;
                    }
                    else
                    {
                        _volUp.Add(source);
                    }
                }
            }
        }
}


/* ----- Code for lerp based [no tested]---------
 
 
 
 *private void Update()
    {
        if (!(Random.value < chanceToChange)) return;
        var loop = loops[Random.Range(0, loops.Count)];
        
        if (_changing.Contains(loop) || untouchables.Contains(loop)) return;
        var endVol = Random.value * Random.value * vol;//little
        var durationFade = Random.Range(2, rangeTimeLapse);
        FadeMusic(loop, endVol, durationFade);
    }
    

    // -------------- Private Methods ------------
    
    private void FadeMusic(AudioSource loop, float endVol, float durationFade)
    {
        
        Assert.IsFalse(_changing.Contains(loop));
        if (Math.Abs(durationFade) < 0.000001f)
        {
            loop.volume = endVol;
        }
        else
        {
            _desired[loop] = endVol;
            _timeLeft[loop] = durationFade;
            _timeStamp[loop] = durationFade;
            _changing.Add(loop);
        }
    }

    private void FixedUpdate()
    {
        
        var sources = new List<AudioSource>(_changing); //new list cause there is removes in the loop
        foreach (var source in sources)
        {
            _timeLeft[source] -= Time.fixedDeltaTime;

            
            var nextVol = Mathf.Lerp( _timeLeft[source]/_timeStamp[source], source.volume, _desired[source]);
            

            source.volume = nextVol;
            
            if (_timeLeft[source] <= 0)
            {
                _changing.Remove(source);
            }
        } 
    }
 * 
 */
}