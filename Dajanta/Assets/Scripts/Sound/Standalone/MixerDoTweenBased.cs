using System.Collections.Generic;
using LovelyInterpolation;
using UnityEngine;
using UnityEngine.Assertions;

namespace Sound.Standalone
{
    internal class MixerDoTweenBased : LoopsManager
    {
        // --------- Variables -------------
        
        private readonly ICollection<AudioSource> _changing = new HashSet<AudioSource>();
        
        // --------- unity Callbacks --------------

        protected override void Validations()
        {
            _changing.Clear();
        }

        private void Start()
        {
            foreach (var audioSource in loops)
            {
                audioSource.Play();
            }

            foreach (var source in startsWithFadeIn)
            {
                var volume = source.volume;
                source.volume = 0;
                FadeMusic(source,volume,Random.Range(5,35));
            } 
        }
        
        private void Update()
        {
            if (!(Random.value < chanceToChange)) return;
            var loop = loops[Random.Range(0, loops.Count)];
            
            if (_changing.Contains(loop) || untouchables.Contains(loop)) return;
            var endVol = Random.value * Random.value * vol;//little
            var durationFade = Random.Range(2, rangeTimeLapse);
            FadeMusic(loop, endVol, durationFade);
        }
        

        // -------------- Private Methods ------------
        
        private void FadeMusic(AudioSource loop, float endVol, float durationFade)
        {
            
            Assert.IsFalse(_changing.Contains(loop));
            loop.InterpolateVol(endVol, durationFade,() => _changing.Remove(loop));
            _changing.Add(loop);
            //tw.SetEase(Ease.InOutQuad);
            
        }
        
    }
}