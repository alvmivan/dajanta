namespace Sound
{
    public interface IMusicManager
    {
        float Vol { get; set; }
        void Stop();
        void Pause();
        void Play();
    }
}