using System.Collections.Generic;
using UnityEngine;

namespace Sound
{
    internal abstract class LoopsManager : MonoBehaviour, IMusicManager
    {
        
        // ---------- Inspector -------------
        
        [Header("Parameters Control")]
        [Space(2)]
        [Range(0,1)]
        [SerializeField] [Tooltip("Max Volume")]
        protected float vol = 1;
        
        [Space(10)]
        [Header("Audio Sources Buckets")]
        [Space(2)]
        
        [SerializeField]
        protected List<AudioSource> loops = new List<AudioSource>();
        [SerializeField] 
        protected List<AudioSource> untouchables = new List<AudioSource>();
        
        [SerializeField] 
        protected List<AudioSource> startsWithFadeIn = new List<AudioSource>();
        
        [Space(10)]
        [Header("Randomness Variables")]
        [Space(2)]
        
        [Tooltip("Chance to make a vol change in a loop")]
        [Range(0, 1)] [SerializeField]
        protected float chanceToChange = .1f; 
        [Tooltip("max time to make the vol change in the loop")]
        [Range(2.1f,15)][SerializeField]
        protected float rangeTimeLapse = 5;
        
        public float Vol
        {
            get => vol;
            set { vol = value;
                AdjustVolumes();
            } 
        }
        public void Stop()
        {
            foreach (var source in loops)
            {
                source.Stop();
            }
        }

        public void Pause()
        {
            foreach (var source in loops)
            {
                source.Pause();
            }
        }

        public void Play()
        {
            foreach (var source in loops)
            {
                source.Play();
            }
        }

        protected void AdjustVolumes()
        {
            vol = Mathf.Clamp01(vol);
            foreach (var source in loops)
            {
                source.volume = Mathf.Clamp(source.volume,vol/2f,vol);
            }
        }
        
        private void OnValidate()
        {
            foreach (var source in new List<AudioSource>(loops))
            {
                if (!source)
                {
                    loops.Remove(source);
                }
            }
            
            foreach (var source in loops)
            {
                source.playOnAwake = false;
                source.loop = true;
            }
            
            
            
            foreach (var source in untouchables)
            {
                if (!loops.Contains(source))
                {
                    loops.Add(source);
                }
            }
            foreach (var source in startsWithFadeIn)
            {
                if (!loops.Contains(source))
                {
                    loops.Add(source);
                }
            }
            AdjustVolumes();
            Validations();
        }

        protected virtual void Validations() {}
        
    }
}