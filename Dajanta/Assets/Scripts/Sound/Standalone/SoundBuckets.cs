using UnityEngine;
// ReSharper disable ConvertToAutoProperty

namespace Sound.Standalone
{
    internal class SoundBuckets : MonoBehaviour
    {
        [SerializeField] private AudioSource shootSound;
        internal static SoundBuckets Instance { get; private set; }
        public AudioSource ShootSound => shootSound;

        private void Awake()
        {
            Instance = this;
        }
    }
}