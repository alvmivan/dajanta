﻿using UnityEngine;

namespace Sound
{
    [ExecuteInEditMode]
    public class ClipName : MonoBehaviour
    {
        private AudioSource source;
        private void OnValidate()
        {
            source = GetComponent<AudioSource>();
        }

        private void Update()
        {
            if (!source) return;
            if (!source.clip) return;
            gameObject.name = $"Track ({source.clip.name})";
        }
    }
}
