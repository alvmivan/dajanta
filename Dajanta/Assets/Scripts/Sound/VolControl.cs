﻿using Initialization;
using UnityEngine;

namespace Sound
{
    public class VolControl : MonoBehaviour
    {
        public AudioSource[] sfxs;
        public AudioSource[] Music;
        

        
        private void Update()
        {
            foreach (var source in Music)
            {
                source.volume = StaticSettings.VolMusic;
            }
            foreach (var source in sfxs)
            {
                source.volume = StaticSettings.VolSxf;
            }
        }
    }
}
