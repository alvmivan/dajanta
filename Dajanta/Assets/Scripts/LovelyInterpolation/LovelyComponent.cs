using UnityEngine;

namespace LovelyInterpolation
{
    internal class LovelyComponent : MonoBehaviour
    {
        private static LovelyComponent _instance;

        public static LovelyComponent Instance
        {
            get
            {
                if (_instance) return _instance;
                var obj = new GameObject("Lovely Object [Don't Touch This]");
                _instance = obj.AddComponent<LovelyComponent>();
                DontDestroyOnLoad(obj);
                return _instance;
            }    
        }
    }
}