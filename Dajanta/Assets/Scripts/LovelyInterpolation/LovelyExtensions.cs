using System;
using System.Collections;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace LovelyInterpolation
{
    public static class LovelyExtensions
    {
        public delegate TModifyType InterpolationFunction<TModifyType>(TModifyType from, TModifyType to, float damp);

        private delegate bool UtilLifeCheck();
        private const float Cronon = 0.001f;
        
        //shortcuts
        public static IActionRunning InterpolatePosition(this Transform transform, Vector3 target, float time=1, Action onComplete=null)
        {
            CheckComplete(ref onComplete);
            if (time <= Cronon)
            {
                transform.position = target;
                onComplete();
                return Dummy;
            }
            var cor = LovelyComponent.Instance.StartCoroutine(Transition(
                transform.position,target,
                value =>transform.position = value,
                Vector3.Lerp,time,onComplete,()=>transform));
            return new Switch(LovelyComponent.Instance,cor);
        }


        public static IActionRunning InterpolateRotation(this Transform transform, Quaternion target, float time = 1,
            Action onComplete = null)
        {
            CheckComplete(ref onComplete);
            if (time <= Cronon)
            {
                transform.rotation = target;
                onComplete();
                return Dummy;
            }

            var cor=LovelyComponent.Instance.StartCoroutine(Transition(
                transform.rotation, target,
                value => transform.rotation = value,
                Quaternion.Lerp, time, onComplete,()=>transform));
            return new Switch(LovelyComponent.Instance,cor);
        }


        public static IActionRunning InterpolateVol(this AudioSource audioSource, float desiredVol, float time = 1,
            Action onComplete = null)
        {
            CheckComplete(ref onComplete);
            if (time <= Cronon)
            {
                audioSource.volume = desiredVol;
                onComplete();
                return Dummy;
            }
            
            var cor = LovelyComponent.Instance.StartCoroutine(Transition(audioSource.volume, desiredVol,
                value => audioSource.volume = value, Mathf.Lerp, time, onComplete,()=>audioSource));
            return new Switch(LovelyComponent.Instance,cor);
        }

        private static readonly IActionRunning Dummy = new DummySwitch();


        public static IActionRunning InterpolateSomething<TModifyType>(this Component relatedComponent,
            TModifyType startValue,
            TModifyType desiredValue,
            Action<TModifyType> onModification,
            InterpolationFunction<TModifyType> interpolation,
            float time,
            Action onComplete=null

        )
        {
            if (onModification == null)
            {
                Debug.LogError("onModification Function empty!, preventive return");
                return Dummy;
            }
            if (interpolation == null)
            {
                Debug.LogError("interpolation Function empty!, preventive return");
                return Dummy;
            }

            if (!relatedComponent)
            {
                Debug.LogError("related component empty!, preventive return");
                return Dummy;
            }
            
            CheckComplete(ref onComplete);
            
            if (time <= Cronon)
            {
                onModification(desiredValue);
                onComplete();
                return Dummy;
            }

            var cor = LovelyComponent.Instance.StartCoroutine(Transition(startValue,desiredValue,onModification,interpolation,time,onComplete,()=>relatedComponent));
            return new Switch(LovelyComponent.Instance,cor);
        }
        
        

        private static IEnumerator Transition<TModifyType>(
            TModifyType startValue,
            TModifyType desiredValue,
            Action<TModifyType> onModification,
            InterpolationFunction<TModifyType> interpolation,
            float time,
            Action onComplete,
            UtilLifeCheck utilLifeCheck
            
        )
        {
            var origin = startValue;
            var completion = 0f;
            while (completion < time)
            {
                if (!utilLifeCheck()) yield break;
                onModification(interpolation(origin, desiredValue, completion / time));
                yield return new WaitForSecondsRealtime(Cronon);
                completion += Cronon;
            }
            onComplete();
        }
        
        /* //no generic
        private static IEnumerator MoveTransform(Transform transform, Vector3 target, float time, Action onComplete)
        {
            var origin = transform.position;
            var completion = 0f;

            while (completion < time)
            {
                transform.position = Vector3.Lerp(origin, target, completion / time);
                yield return new WaitForSecondsRealtime(Cronon);
                completion += Cronon;
            }
        }
        
*/
        private static void CheckComplete(ref Action action)
        {
            if (action == null)
            {
                action = () => { };
            }
        }

        public static void WaitAndDo(Action func, float seconds)
        {
            if (func == null) return;
            if (seconds <= Cronon)
            {
                func();
                return;
            }

            LovelyComponent.Instance.StartCoroutine(Waiter(func, seconds));
        }

        private static IEnumerator Waiter(Action action, float seconds)
        {
            yield return new WaitForSecondsRealtime(seconds);
            action();
        }
        
    }

    public class DummySwitch : IActionRunning
    {
        public void Interrupt(){}
    }
}