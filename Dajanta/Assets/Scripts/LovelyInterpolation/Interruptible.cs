using UnityEngine;

namespace LovelyInterpolation
{
    internal class Switch : IActionRunning
    {
        private MonoBehaviour CoroutineParent;
        private Coroutine ToInterrupt;

        public Switch(MonoBehaviour coroutineParent, Coroutine interrupt)
        {
            CoroutineParent = coroutineParent;
            ToInterrupt = interrupt;
        }
        
        public void Interrupt()
        {
            CoroutineParent.StopCoroutine(ToInterrupt);
        }
    }

    public interface IActionRunning
    {
        void Interrupt();
    }
}