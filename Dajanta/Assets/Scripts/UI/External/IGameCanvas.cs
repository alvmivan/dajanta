using GamePlay;
using UnityEngine;

namespace UI.External
{
    public interface IGameCanvas
    {
        void Aim(bool showAim);
        void UpdateData(IMatchState match);
        void LostDuck();

        void DrawJoyStick(Vector2 origin, Vector2 touchPoint);

        void QuitJoyStick();

    }
}