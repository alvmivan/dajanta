namespace UI.External
{
    // ReSharper disable once InconsistentNaming
    public static class CanvasAPI
    {
        public static IGameCanvas Canvas { get; } = new GameCanvasHandler();
    }
}