using System.Collections;
using GamePlay;
using UI.External;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal class GameCanvasHandler : IGameCanvas
    {
        
        private static CanvasComponent Helper => CanvasComponent.Instance;
        
        public void Aim(bool showAim)
        {
            var color = new Color(1,1,1,showAim?1:0);
            WaitAndColor(Helper.Aim(),color, .1f);
        }
        
        
        

        public void UpdateData(IMatchState match)
        {
            Helper.LogDucksField().text = $"Ducks: {match.Ducks()}";
            Helper.LogBulletsField().text = $"Bullets: {match.BulletsInCharger()}";
            Helper.ScoreField().text = $"{match.Score()}";
        }

        public void LostDuck()
        {
            Helper.OutOfRangeImage().color = new Color(1f, 1f, 0.95f,1);
            WaitAndColor(Helper.OutOfRangeImage(),new Color(0,0,0,0), 2);
        }

        public void DrawJoyStick(Vector2 origin, Vector2 touchPoint)
        {
            Helper.JoystickOrigin().enabled = true;
            Helper.JoystickEnd().enabled = true;
            Helper.JoystickOrigin().transform.position = origin;
            Helper.JoystickEnd().transform.position = touchPoint;
        }

        public void QuitJoyStick()
        {
            Helper.JoystickOrigin().enabled = false;
            Helper.JoystickEnd().enabled = false;
        }

        private static IEnumerator ColorTime(Graphic image, Color color, float time)
        {
            yield return new WaitForSecondsRealtime(time);
            image.color = color;

        }
        private static void WaitAndColor(Graphic image, Color color, float time) => Helper.StartCoroutine(ColorTime(image, color, time));
    }
}