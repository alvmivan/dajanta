using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal class CanvasHandler : CanvasComponent
    {

        [SerializeField] private TextMeshProUGUI ducksText;
        
        [SerializeField] private TextMeshProUGUI bulletsText;
        
        [SerializeField] private TextMeshProUGUI scoreField;
        
        [SerializeField] private Image popupOutOfRange;
        
        [SerializeField] private Image crossHair;
        [SerializeField] private Image joyStickFrom;
        [SerializeField] private Image joyStickTo;

        public override Image JoystickEnd() => joyStickTo;
        public override Image JoystickOrigin() => joyStickFrom;
        
        
        public override TMP_Text LogDucksField() => ducksText;

        public override TMP_Text LogBulletsField() => bulletsText;
        public override TMP_Text ScoreField() => scoreField;

        public override Image Aim() => crossHair;

        public override Image OutOfRangeImage() => popupOutOfRange;
    }
}