using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal abstract class CanvasComponent : MonoBehaviour
    {
        public static CanvasComponent Instance { get; private set; }
        public abstract Image JoystickEnd();
        public abstract Image JoystickOrigin();

        private void Awake()
        {
            Instance = this;
        }


        public abstract TMP_Text LogDucksField();
        public abstract TMP_Text LogBulletsField();
        
        public abstract TMP_Text ScoreField();

        public abstract Image Aim();

        public abstract Image OutOfRangeImage();
    }
}