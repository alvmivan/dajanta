using CameraSys.Exposed;
using UnityEngine;

namespace CameraSys.Internal
{
    public class CameraApi : ICameraSystem
    {
        public Vector3 CameraPosition => CameraBehaviour.Instance.CameraPosition;
        public Vector3 CameraDirection => CameraBehaviour.Instance.CameraDirection;
    }
}