using UnityEngine;

namespace CameraSys.Internal
{
    internal interface ICameraBehaviour
    {
        Vector3 CameraPosition { get; }
        Vector3 CameraDirection { get; }
    }
}