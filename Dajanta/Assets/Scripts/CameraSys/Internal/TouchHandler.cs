using System;
using UI.External;
using UnityEngine;

namespace CameraSys.Internal
{
    internal class TouchHandler : ICameraRotationHandler
    {

        private static Vector2 Min => Vector2.zero;
        private static Vector2 Max => new Vector2(Screen.width, Screen.width) * .5f;

        private const float XMultiplier = 30f;
        private const float YMultiplier = -25f;
        private const float Smooth = 0.6f;

        private Vector2 _firstPoint = Vector2.zero;
        private Vector2 _currentPoint = Vector2.zero;
        private Vector2 _currentDirection = Vector2.zero;
        
        private static IGameCanvas Canvas => CanvasAPI.Canvas;
        
        
        public void HandleRotation(Transform xAxis, Transform yAxis)
        {
            //works with just 2 touches in screen
            
            if (Input.touchCount <= 0) return;
            var touch = Input.GetTouch(0);
            if (!OnRect(touch.position, Min, Max))
            {
                if (Input.touchCount <= 1)
                {
                    ReleaseJoystick();
                    return;
                }
                touch = Input.GetTouch(1);
                if (!OnRect(touch.position, Min, Max))
                {
                    ReleaseJoystick();
                    return;
                }
            }
            if (touch.phase == TouchPhase.Began)
            {
                _firstPoint = touch.position;
                return;
            }
            if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                _firstPoint = Vector2.zero;
                ReleaseJoystick();
                return;
            }

            _currentPoint = touch.position;
            Canvas.DrawJoyStick(_firstPoint,_currentPoint);
            
            /*if(Math.Abs(_currentDirection.sqrMagnitude) < 0.01f)
            {
                _firstPoint = _currentPoint;
            }
            */
            if (touch.phase == TouchPhase.Stationary || touch.deltaPosition.sqrMagnitude <= 0.01f)
            {
                _currentDirection *= .9f;
            }
            else
            {
                _currentDirection = Vector2.Lerp((_currentPoint - _firstPoint).normalized, _currentDirection, Smooth);
            }

            var delta = _currentDirection * Time.fixedDeltaTime;
            delta.x *= XMultiplier;
            delta.y *= YMultiplier;
            xAxis.Rotate(delta.y,0,0);
            yAxis.Rotate(0,delta.x,0);
        }

        private void ReleaseJoystick()
        {
            _currentDirection = Vector2.Lerp(_currentDirection, Vector2.zero, Smooth / 10);
            Canvas.QuitJoyStick();
        }

        private static bool OnRect(Vector2 value, Vector2 min, Vector2 max) => value.x < max.x && value.y < max.y && value.x > min.x && value.y > min.y;
    }
}