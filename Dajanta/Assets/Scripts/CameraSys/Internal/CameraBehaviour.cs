﻿
using UnityEngine;

namespace CameraSys.Internal
{
    
    internal class CameraBehaviour : MonoBehaviour , ICameraBehaviour
    {
        // ---------- MonoBehaviour Singleton Stuff ---------------
        internal static ICameraBehaviour Instance { get; private set; }//requires component active

        private void OnEnable()
        {
            Instance = this;
        }

        // ---------- Inspector -----------
        
        [SerializeField] private Transform CameraTransform;
        [SerializeField] private Transform CameraXAxis;
        [SerializeField] private Transform CameraYAxis;
        [SerializeField] private Transform CameraZAxis;

        
        // ---------- Variables ----------
        private ICameraRotationHandler _handler;
        
        // ------- Unity callbacks --------
        
        /// <summary>
        ///  here handles internal dependencies
        /// </summary>
        private void Awake()
        {
            Instance = this;
            #if UNITY_EDITOR
            _handler = new LaptopHandler();
            #else
            if (SystemInfo.supportsGyroscope)
            {
                Input.gyro.enabled = true;
                _handler = new GyroscopeHandler(CameraXAxis,CameraTransform);
            }
            else
            {
                _handler = new TouchHandler();
            }
            #endif
        }

        
        private void FixedUpdate()
        {
            _handler.HandleRotation(CameraXAxis,CameraYAxis);
        }
        
        // ---------------- Public Properties ----------

        public Vector3 CameraPosition => CameraTransform.position;
        public Vector3 CameraDirection => CameraTransform.forward;
    }
}


