using UnityEngine;

namespace CameraSys.Internal
{
    internal class LaptopHandler : ICameraRotationHandler
    {    
        private const float XMultiplier = 40f;
        private const float YMultiplier = 40f;
        public void HandleRotation(Transform xAxis, Transform yAxis)
        {
            var delta = new Vector2(
                            Input.GetAxis("Horizontal"),
                            -Input.GetAxis("Vertical")
                        ) * Time.fixedDeltaTime;
            delta.x *= XMultiplier;
            delta.y *= YMultiplier;
            xAxis.Rotate(delta.y,0,0);
            xAxis.Rotate(0,delta.x,0);
        }
    }
}