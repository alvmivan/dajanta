using UnityEngine;

namespace CameraSys.Internal
{
    internal class GyroscopeHandler : ICameraRotationHandler
    {
        private const float DampingRotation = 0.75f;
        private readonly Gyroscope gyro;
        private static readonly Quaternion _z1 = new Quaternion(0, 0, 1, 0);
        private readonly Transform _cameraTransform;
        public GyroscopeHandler(Transform cameraContainer, Transform cameraTransform)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            cameraContainer.position = cameraTransform.position;
            cameraTransform.SetParent(cameraContainer);
            cameraContainer.rotation = Quaternion.Euler(90, 90, 0);
            _cameraTransform = cameraTransform;
        }
        
        
        public void HandleRotation(Transform xAxis, Transform yAxis)
        {
            gyro.enabled = true; // necessary
            var rot = gyro.attitude * _z1;
            _cameraTransform.localRotation = Quaternion.Lerp(_cameraTransform.localRotation,rot,DampingRotation);
            //xAxis must be the external
        }
    }
}