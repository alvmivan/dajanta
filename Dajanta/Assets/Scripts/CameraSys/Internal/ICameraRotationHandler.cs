using UnityEngine;

namespace CameraSys.Internal
{
    internal interface ICameraRotationHandler
    {
        void HandleRotation(Transform xAxis, Transform yAxis);
    }
}