using CameraSys.Internal;

namespace CameraSys.Exposed
{
    public class CameraSystem
    {
        //requires CameraBehaviour in scene
        public static ICameraSystem API { get; } = new CameraApi();
    }
}