using UnityEngine;

namespace CameraSys.Exposed
{
    public interface ICameraSystem
    {
        Vector3 CameraPosition { get; }
        Vector3 CameraDirection { get; }
    }
}