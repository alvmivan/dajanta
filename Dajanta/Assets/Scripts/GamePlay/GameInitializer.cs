using Achievements;
using Dog;
using Ducks.Exposed;
using Sound;
using UI.External;
using UnityEngine;

namespace GamePlay
{
    internal class GameInitializer : MonoBehaviour
    {
        [SerializeField] private AbstractGameManager gameManager;
        
        [SerializeField] private AbstractDogHandler dogHandler;
        
        
        
        private void Start()
        {
            OnLoad();
        }
       

        private void OnLoad()
        {
            var persistentData = new PersistentData();
            
            gameManager.TakeDependencies(
                dogHandler,
                DuckSystem.API,
                Shooting.Exposed.ShootingSystem.API,
                SoundManager.SoundSystem,
                new MatchState(persistentData), 
                CanvasAPI.Canvas,
                Achievements.Scores.GetHandler()
            );
            gameManager.StartGame();
        }
        
/*
        private void SaveData()
        {
            
        }  

      private void OnApplicationQuit()
        {
            SaveData();
        }

      

      private void OnDestroy()
        {
            SaveData();
        }

        private void OnDisable()
        {
            SaveData();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if(!hasFocus)
                SaveData();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                SaveData();
            }
        }

*/
        
    }
}