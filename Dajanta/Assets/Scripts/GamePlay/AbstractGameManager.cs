using Achievements;
using Dog;
using Ducks.Exposed;
using Shooting.Exposed;
using Sound;
using UI.External;
using UnityEngine;

namespace GamePlay
{
    public abstract class AbstractGameManager : MonoBehaviour
    {
        public abstract void TakeDependencies
        (AbstractDogHandler dogHandler, IDuckBehaviourApi ducks,
            IShootingApi shootingApi, ISoundSystem soundSystem,
            IMatchStateWritable match, IGameCanvas gameCanvas, IAchievementsHandler achievementsHandler);

        public abstract void StartGame();
    }
}