using Ducks.Exposed;

namespace GamePlay
{
    internal class MatchState : IMatchStateWritable
    {

        private readonly IScoreContainer _persistentData;
        private int _ducks = 0;
        private int _bullets = 4;

        public MatchState(IScoreContainer persistentData)
        {
            _persistentData = persistentData;
        }

        public int Score()
        {
            return _persistentData.Score();
        }

        public int Ducks() => _ducks;

        public int BulletsInCharger() => _bullets;

        public void SpendBullet()
        {
            _bullets--;
        }

        public void Reload()
        {
            _bullets = 4;
        }

        public void CatchDuck(IDuck duck)
        {
            _ducks++;
            _persistentData.IncScore(duck.Price.Value);
        }
    }

    public interface IMatchStateWritable : IMatchState
    {
        
        void SpendBullet();
        void Reload();
        void CatchDuck(IDuck duck);
    }

    public interface IMatchState
    {
        int Score();
        int Ducks();
        int BulletsInCharger();
    }
}