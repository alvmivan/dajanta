﻿using System.Collections;
using Achievements;
using Dog;
using Ducks.Exposed;
using Shooting.Exposed;
using Sound;
using UI.External;
using UnityEngine;

namespace GamePlay
{
    internal class GameManager : AbstractGameManager
    {

        [SerializeField] private Vector3 FirstTarget;
        [SerializeField] private Vector3 LakePoint;
        [Tooltip("Its relative to the transform of this object")]
        [SerializeField] private float MaxHeightDuck;//relative 

        #if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(FirstTarget,Vector3.one);
            Gizmos.color = Color.green;
            Gizmos.DrawCube(LakePoint,Vector3.one);
            Gizmos.color = new Color(0,1,1);
            Gizmos.DrawLine(FirstTarget,LakePoint);
            
            Gizmos.DrawCube(transform.position+Vector3.up*MaxHeightDuck,new Vector3(40,1,40));
            
        }
        #endif
       
        

        private AbstractDogHandler _dog;
        private IDuckBehaviourApi _ducksSystem;
        private IShootingApi _shooting;
        private ISoundSystem _soundSystem;
        private IMatchStateWritable _match;
        private IGameCanvas _gameCanvas;
        private IAchievementsHandler _achievements;
        
        public override void TakeDependencies
            // todo : move parameters into an structure.. ? necessary? .. this is the structure
            (AbstractDogHandler dogHandler, IDuckBehaviourApi ducks,
                IShootingApi shootingApi, ISoundSystem soundSystem,
                IMatchStateWritable match, IGameCanvas gameCanvas, IAchievementsHandler achievementsHandler)
        {
            _dog = dogHandler;
            _ducksSystem = ducks;
            _shooting = shootingApi;
            _soundSystem = soundSystem;
            _match = match;
            _gameCanvas = gameCanvas;
            _achievements = achievementsHandler;
        }

        public override void StartGame()
        {
            StartCoroutine(Intro());
            
        }

        private IEnumerator Intro()
        {
            _runningMatch = true;
            //_dog.Walk(true);
            _dog.Sniff(true);
            _dog.Tail = TailState.Alert;
            //_dog.MoveTarget(FirstTarget);
            //yield return new WaitUntil(()=>Vector3.Distance(_dog.transform.position,FirstTarget)<0.5f);
            yield return new WaitForSecondsRealtime(1f);
            _dog.Tail = TailState.Happy;
            _dog.Sniff(false);
            _dog.Walk(false);
            _dog.Head = HeadDirection.Left;
            yield return new WaitForSecondsRealtime(1.2f);
            _dog.TripleBark();
            yield return new WaitForSecondsRealtime(1.2f);
            _dog.Head = HeadDirection.Front;
            //_dog.JumpTo(LakePoint);
            yield return new WaitForSecondsRealtime(2.5f);
            //_dog.Remove();
            _runningMatch = false;
        }

        private bool _runningMatch=true;
        
        private IEnumerator Match()
        {
            print("spawn");
            _ducksSystem.SpawnDuck();
            if (_runningMatch) yield break;
            _runningMatch = true;
            var end = false;
            _gameCanvas.Aim(true);
            while (!end)
            {
                _dog.Tail = TailState.Alert;
                var letEscape = false;
                while (!_shooting.Shoots() && !letEscape)
                {
                    if (_ducksSystem.DuckPosition.y > transform.position.y+MaxHeightDuck)
                    {
                        _achievements.ShootMiss();
                        yield return null;
                        print("duck goes no shoot");
                        yield return null;
                        print("goes");
                        _ducksSystem.LetEscape();
                        _gameCanvas.LostDuck();
                        _gameCanvas.Aim(false);
                        end = true;
                        letEscape = true;
                        _dog.Tail = TailState.Idle;
                    }
                    else
                    yield return null;
                }
                if (end)
                {
                    _gameCanvas.UpdateData(_match);
                    yield return new WaitForSecondsRealtime(.1f);
                }
                else
                {
                    _soundSystem.Shoot();
                    _match.SpendBullet();
                    _gameCanvas.UpdateData(_match);
                    if (_shooting.AimingCorrectly())
                    {
                        
                        _dog.Tail = TailState.Happy;
                        print("shot");
                        var current = _ducksSystem.CurrentDuck();
                        _match.CatchDuck(current);
                        _ducksSystem.KillDuck(current);
                        var sinks = false;
                        _ducksSystem.OnDuckSinks += duck => sinks = true;
                        _gameCanvas.UpdateData(_match);
                        _gameCanvas.Aim(false);
                        yield return new WaitUntil(() => sinks);
                        _dog.Head = HeadDirection.Right;
                        yield return new WaitForSecondsRealtime(2f);
                        _dog.SimpleBark();
                        yield return new WaitForSecondsRealtime(1.2f);
                        _dog.Head = HeadDirection.Front;
                        end = true;
                        _achievements.ShootOk(current.Price.Value);
                    }
                    else
                    {
                        _achievements.ShootMiss();
                        if (_shooting.IsAiming(_dog.Layer))
                        {
                            _dog.Dialog("WTF Man?",2);
                        }
                        if (_match.BulletsInCharger() <= 0)
                        {
                            print("goes");
                            _ducksSystem.FailShoot();
                            var goes = false;
                            _ducksSystem.OnDuckGoesAway += duck => goes = true;
                            _gameCanvas.LostDuck();
                            _gameCanvas.Aim(false);
                            yield return new WaitUntil(() => goes);
                            end = true;
                            _dog.Tail = TailState.Idle;
                        }
                    }
                    _gameCanvas.UpdateData(_match);
                    yield return new WaitForSecondsRealtime(.5f);
                }
            }
            _match.Reload();
            _gameCanvas.Aim(true);
            _runningMatch = false;
        }

        private void Update() // loop-based game is placeholder
        {
            if(_match!=null)
                _gameCanvas.UpdateData(_match);
            if (!_runningMatch)
                StartCoroutine(Match());
        }
    }
}

/* Time Line
 * dog sniff - move forward
 * 3 barf
 * jumps into lake (swim random?)
 * ducks spawn
 *
 * while not exit match
 *      case shoot ok :
 *          duck falls into water
 *      case fail shoot
 *          duck goes away
 *          wait until duck out of range
 * 
 *      put duck into water again
 *      restart duck path
 * on exit match:
 *     show scores (or money)
 
 * 
 */