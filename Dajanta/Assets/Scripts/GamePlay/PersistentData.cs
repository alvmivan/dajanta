using System;
using System.Text;

namespace GamePlay
{
    [Serializable]
    internal class PersistentData : IScoreContainer
    {
        public string code = "DH";
        private StringBuilder _builder = new StringBuilder();

        public int Score() => code.Length - 2;

        public void SetScore(int value){
            
            _builder.Clear();
            _builder.Append("DH");
            for (int i = 0; i <= value; i++)
            {
                var c = (char) (i + 'a');
                _builder.Append(c);
            }
            code = _builder.ToString();
            
        }

        public void IncScore(int incValue)
        {
            SetScore(Score()+incValue);
        }

        
    }

    internal interface IScoreContainer 
    {
        int Score();
        void SetScore(int value);
        void IncScore(int incValue);
    }
}