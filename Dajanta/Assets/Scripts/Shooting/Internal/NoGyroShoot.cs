using System.Linq;
using UnityEngine;

namespace Shooting.Internal
{
    internal class NoGyroShoot : IShootingOption
    {
        public bool Shoot()
        {
            return Input.touches.Any(Ask);
        }
        private static bool Ask(Touch t)
        {
            //if touch starts and occurs in the right half of the screen
            return t.phase == TouchPhase.Began && t.position.x > Screen.width / 2f;
        }
    }
}