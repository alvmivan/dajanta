using System.Linq;
using UnityEngine;

namespace Shooting.Internal
{
    internal class GyroShoot : IShootingOption
    {
        public bool Shoot()
        {
            return Input.touches.Any(t=>t.phase==TouchPhase.Began);
        }
    }
}