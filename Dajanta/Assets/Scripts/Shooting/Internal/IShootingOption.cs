namespace Shooting.Internal
{
    internal interface IShootingOption
    {
        bool Shoot();
    }
}