using UnityEngine;

namespace Shooting.Internal
{
    internal class LaptopTester : IShootingOption
    {
        public bool Shoot()
        {
            return Input.GetKeyDown(KeyCode.Space);
        }
    }
}