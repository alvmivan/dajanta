using System.Linq;
using CameraSys.Exposed;
using Ducks.Exposed;
using Shooting.Exposed;
using UnityEngine;

namespace Shooting.Internal
{
    internal class ShootingApi : IShootingApi
    {
        // ------- Variables and Constants --------
        private const int MaxHitsToScan = 20;
        private const float MaxDistanceRayCast = float.PositiveInfinity;
        private readonly RaycastHit[] _rayCastHits = new RaycastHit[MaxHitsToScan];

        private readonly IShootingOption _shootHandler;
        
        // ------------- Constructors -------------------

        public ShootingApi()
        {
#if UNITY_EDITOR
            _shootHandler = new LaptopTester();      
#else
            _shootHandler = SystemInfo.supportsGyroscope ? (IShootingOption) new GyroShoot() : new NoGyroShoot();
#endif
        }

        
        
        // --------- Public Properties ----------
        
        public float TargetDistance
        {
            get
            {
                var direction = CameraSystem.API.CameraDirection;
                var source = CameraSystem.API.CameraPosition;
                var resultsCount = Physics.RaycastNonAlloc(source, direction, _rayCastHits, MaxDistanceRayCast);
                return resultsCount>0 ? source.GoTo(_rayCastHits.OrderBy(rh=>rh.distance).First().point).magnitude : float.PositiveInfinity;
            }
        }
        
        
        // -------------- Public Methods -----------
        
        
        public bool Shoots()
        {
            return _shootHandler.Shoot();
        }

        public bool AimingCorrectly()
        {
            var layerMask = DuckSystem.API.DucksLayerMask;
            var direction = CameraSystem.API.CameraDirection;
            var source = CameraSystem.API.CameraPosition;
            var resultsCount = Physics.RaycastNonAlloc(source, direction, _rayCastHits, MaxDistanceRayCast, layerMask);
            return resultsCount > 0;
        }

        public bool IsAiming(LayerMask layer)
        {
            var layerMask = layer;
            var direction = CameraSystem.API.CameraDirection;
            var source = CameraSystem.API.CameraPosition;
            var resultsCount = Physics.RaycastNonAlloc(source, direction, _rayCastHits, MaxDistanceRayCast, layerMask);
            return resultsCount > 0;
        }
    }
}