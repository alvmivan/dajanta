﻿using Shooting.Internal;

namespace Shooting.Exposed
{

    public class ShootingSystem
    {
        internal static readonly ShootingApi _api = new ShootingApi();
        public static IShootingApi API => _api;
    }
}
