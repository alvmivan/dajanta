using UnityEngine;

namespace Shooting.Exposed
{
    public interface IShootingApi
    {
        
        float TargetDistance { get; }
        bool Shoots();

        bool AimingCorrectly();
        bool IsAiming(LayerMask layer);
    }
}