﻿using UnityEngine;

namespace Ducks.Internal.Behavior
{
    internal class DuckMovement : DuckKinematics
    {

        [Range(0,50)]private float speed = 10;

        public override Vector3 Direction { get; set; } = Vector3.zero;

        public override float Speed { get=>speed; set=>speed=value; }

        private int _level = 1;
        public override void SetLevel(int level)
        {
            _level = level;
        }

        private void FixedUpdate()
        {
            if (Direction == Vector3.zero) return;
            transform.position += Direction * Time.fixedDeltaTime * speed * _level;
            var rotation = transform.rotation;
            transform.up = Vector3.up;
            transform.forward = Direction;
            var rotationDesired = transform.rotation;
            transform.rotation = Quaternion.Lerp(rotation,rotationDesired,0.5f);

        }
    }
}
