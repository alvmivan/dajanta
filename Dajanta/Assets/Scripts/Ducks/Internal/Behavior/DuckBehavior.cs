using ADTs;
using Ducks.Exposed;

using UnityEngine;

namespace Ducks.Internal.Behavior
{
    internal abstract class DuckBehavior : MonoBehaviour , IDuck
    {
        
        
        // ------------ variables -------------
        private static int IDgen = 0;
        
        // ------------- Inspector ----------
        
        [SerializeField] protected Animator animator;

        [SerializeField] protected Currency price = 1;
        
        // --------------- public properties ------------

        public ICurrency Price => price;
        public int ID { get; } = IDgen++;
        
        public IDuckType DuckType { get; } = new CommonDuck();//placeholder        
        public abstract Transform DuckTransform { get; }

        // ------------ Abstract Methods ----------
        
        public abstract void Kill();
        public abstract void GoAway();
        public abstract void Spawn();


        public abstract void LetEscape();
    }
}