using UnityEngine;

namespace Ducks.Internal.Behavior
{
    internal abstract class DuckKinematics : MonoBehaviour
    {
        
        public abstract Vector3 Direction { get; set; }
        public abstract float Speed { get; set; }

        public abstract void SetLevel(int level);
    }
}