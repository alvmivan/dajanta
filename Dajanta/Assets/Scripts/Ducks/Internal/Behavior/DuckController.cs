﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Ducks.Exposed;

using UnityEngine;
using Random = UnityEngine.Random;

namespace Ducks.Internal.Behavior
{
    internal class DuckController : DuckBehavior
    {
        
        
        
        // --------- Constants ---------
        
        private const float SpeedOnGoesAway = 2;
        private const float WaterDepth = 20f;
        
        // -------- Editor ----------
        
        [SerializeField] private DuckKinematics duck;
        
        [SerializeField] private VectorsRestraintField generator;

        [SerializeField] private float minDistanceTarget = 0.5f;

        [SerializeField] private float heightToGoAway = 50;

                
        // -------- Variables -------------
        
        private IList<Vector3> _currentList = new List<Vector3>();
        private int _index = 0;
        
        // ----------- Unity Callbacks ---------


        private void Update()
        {
            if (_index < _currentList.Count)
            {
                var currentTarget = _currentList[_index];
                duck.Direction = (currentTarget - duck.transform.position).normalized;
                if (Vector3.Distance(duck.transform.position, currentTarget) < minDistanceTarget)
                {
                    _index++;
                }
            }
            else
            {
                if(_currentList.Count>1)
                {
                    duck.Direction = Vector3.up * 4;
                }
                if (duck.transform.position.y > heightToGoAway)
                {
                    DuckSystem.Api.OnDuckGoesAwayInvoker(this);
                }
                
            }
            
        }
#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            var point = transform.position;
            point.y = heightToGoAway;
            Gizmos.color = new Color(0.67f, 0.69f, 0.13f, 0.15f);
            Gizmos.DrawCube(point,new Vector3(102,2,102));
            Gizmos.color = new Color(0.57f, 0.79f, 0.23f, 1f);
            Gizmos.DrawWireCube(point,new Vector3(102,2,102));
            
            //invert if statement to reduce nesting
            if (_currentList == null || !_currentList.Any() || _currentList.Count <= _index) return;
            Gizmos.color = new Color(0.56f, 1f, 0.73f, 0.4f);
            Gizmos.DrawSphere(_currentList[_index], 2);
            Gizmos.color = new Color(1f, 0.99f, 0.14f, 0.7f);
            Gizmos.DrawLine(_currentList[_index], duck.transform.position);
        }
#endif
        // ----------------- Public Methods ----------------


        public override void LetEscape()
        {
            _currentList = new List<Vector3>();
            duck.Direction = Vector3.down;
            var duckTransform = duck.transform;
            var duckPosition = duckTransform.position;
            duckPosition.y = generator.Floor - WaterDepth;
            duckTransform.position = duckPosition;
        }

        public override void GoAway()
        {
            _currentList = new List<Vector3>();
            duck.Direction = Vector3.up * SpeedOnGoesAway;
        }

        public override void Spawn()
        {
            BeginDuck();
        }


        public override Transform DuckTransform => duck.transform;

        public override void Kill()
        {
            StartCoroutine(KillDuck());
        }
        
        // ------------------ Private Methods -------------
        
        private void BeginDuck()
        {
            _index = 0;
            price = Random.Range(1, 5);
            duck.SetLevel(price.Value);
            _currentList = generator.Points();
            var duckTransform = duck.transform;
            var duckPosition = duckTransform.position;
            duckPosition.y = generator.Floor - WaterDepth;
            duckTransform.position = duckPosition;
        }
        
        private IEnumerator KillDuck()
        {
            //Falls And Rotates
            //TODO: change animator state
            const float duckRotation = 50;
            _currentList = new List<Vector3>();
            duck.Direction = Vector3.down;
            
            while (duck.transform.position.y > generator.Floor - WaterDepth)
            {
            //    duck.Direction = Vector3.down * duck.Speed * Time.deltaTime;//with WaitForEndOfFrame works like in an Update
                duck.transform.Rotate(0,0,duckRotation);
                yield return new WaitForEndOfFrame();
            }
            DuckSystem.Api.OnDuckSinksInvoker(this);
        }
    }
}
