using Ducks.Exposed;

namespace Ducks.Internal.Behavior
{
    
    /// <summary>
    /// At the moment it is a placeholder, in the future it will be used to identify the type of ducks,
    /// and then map them with a 3D model that will be displayed in the duck gallery.
    /// </summary>
    internal class CommonDuck : IDuckType
    {
        
    }
}