﻿using System.Collections.Generic;
using UnityEngine;

namespace Ducks.Internal.Behavior
{
    internal abstract class VectorsRestraintField : MonoBehaviour
    {
        public abstract float Floor { get; }
        public abstract float Top { get; }

        public abstract IList<Vector3> Points();

    }
}