using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
    using UnityEditor;
#endif
using UnityEngine;

namespace Ducks.Internal.Behavior
{
    internal class PointsInCylinder : VectorsRestraintField
    {
        [SerializeField] private Vector3 bottomCenter =Vector3.zero;
        [SerializeField] private float height;
        [SerializeField] private float radius;
        [SerializeField] private int points;


        private Vector3 GetPoint()
        {
            var h = Random.Range(0, height);
            var v2 = Random.insideUnitCircle * Random.value * radius;
            return new Vector3(v2.x,h,v2.y) + bottomCenter;
        }

        public override float Floor => bottomCenter.y;
        public override float Top => bottomCenter.y + height;

        public override IList<Vector3> Points()
        {    
            var list = new List<Vector3>();
            for (var i = 0; i < points; i++)
            {
                list.Add(GetPoint());
            }
            var listSorted= list.OrderBy(v=>v.y).ToList();
            listSorted.Add(listSorted[listSorted.Count-1]+Vector3.up*2);
            return listSorted;
        }


        private void OnValidate()
        {
            ValidateGameVariables();
#if UNITY_EDITOR
            //ValidateEditorVariables();
#endif
        }

        

        private void ValidateGameVariables()
        {
            if (bottomCenter == Vector3.zero)
                bottomCenter = transform.position;
            radius = Mathf.Max(0.01f, radius);
            height = Mathf.Max(0.01f, height);
        }

#if UNITY_EDITOR
        [SerializeField] private float cutsGizmo;
#endif
        private void ValidateEditorVariables()
        {
            //cutsGizmo = (int)Mathf.Max(2, cutsGizmo);
        }
        
        private void OnDrawGizmos()
        {
            

            var vectors = new List<Vector3>();

            for (var i = 0f; i < 2; i+=0.05f)
            {
                vectors.Add(Vector2.up.Rotate(i));
            }

#if UNITY_EDITOR
            Handles.color = Color.red;
            
            Handles.DrawWireDisc(bottomCenter,Vector3.up, radius);
            Handles.DrawWireDisc(bottomCenter+Vector3.up*height,Vector3.up, radius);
            
            
            for (var i = 0f; i < 1f; i += 1 / cutsGizmo)
            {
                Handles.color = new Color(0.31f, 0.46f, 1f, 0.92f);
                Handles.DrawWireDisc(bottomCenter+Vector3.up*height*i,Vector3.up, radius);
                Handles.color = new Color(0.21f, 0.66f, .4f, 0.915f);
                Handles.DrawWireDisc(bottomCenter+Vector3.up*height,Vector3.up, radius*i);
                Handles.DrawWireDisc(bottomCenter,Vector3.up, radius*i);
            }
            
            
#endif
        
            foreach (var m in vectors)
            {
                var p = new Vector3(m.x,0,m.y);
                var p2= new Vector3(-m.x,0,-m.y);
                p *= radius;
                p2 *= radius;
                var position = bottomCenter;
                p += position;
                p2 += position;
                Gizmos.color = new Color(0f, 0f, 1f, 0.22f);
                Gizmos.DrawLine(p,p+Vector3.up*height);
                Gizmos.color = new Color(0.22f, 1f, 0.29f, 0.05f);
                Gizmos.DrawLine(p2+Vector3.up*height,p+Vector3.up*height);
                Gizmos.color = new Color(1f, 0.27f, 0.37f, 0.09f);
                Gizmos.DrawLine(p2,p);
            }
        }
    }
}


