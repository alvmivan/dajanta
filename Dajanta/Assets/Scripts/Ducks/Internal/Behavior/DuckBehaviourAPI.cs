using Ducks.Exposed;
using UnityEngine;

namespace Ducks.Internal.Behavior
{
    internal class DuckBehaviourApi : IDuckBehaviourApi
    {
        public event DuckBasedFunction OnDuckGoesAway;
        public event DuckBasedFunction OnDuckSinks;

        public LayerMask DucksLayerMask => DuckSystemBuckets.Instance.DucksLayerMask;
        


        public void SpawnDuck()
        {
            DuckSystemBuckets.Instance.Controller.Spawn();
        }

        public IDuck CurrentDuck()
        {
            return DuckSystemBuckets.Instance.Controller;
        }

        public Vector3 DuckPosition => DuckSystemBuckets.Instance.Controller.DuckTransform.transform.position;

        public void KillDuck(IDuck duck)
        {
            //with that system (one duck per match) I don't need the duck
            //but it will work with more ducks and the interface is ok
            DuckSystemBuckets.Instance.Controller.Kill();
        }

        public void LetEscape()
        {
            DuckSystemBuckets.Instance.Controller.LetEscape();
        }

        public void FailShoot()
        {
            DuckSystemBuckets.Instance.Controller.GoAway();
        }

        internal void OnDuckSinksInvoker(IDuck duck)
        {
            OnDuckSinks?.Invoke(duck);
        }
        internal void OnDuckGoesAwayInvoker(IDuck duck)
        {
            OnDuckGoesAway?.Invoke(duck);
        }
    }
}