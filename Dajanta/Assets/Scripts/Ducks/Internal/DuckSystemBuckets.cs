using Ducks.Internal.Behavior;
using UnityEngine;
// ReSharper disable ConvertToAutoProperty

namespace Ducks.Internal
{
    internal class DuckSystemBuckets : MonoBehaviour
    {
        [SerializeField] private LayerMask ducksLayerMask;
        [SerializeField] private DuckBehavior controller;
        
        
        public LayerMask DucksLayerMask => ducksLayerMask;
        public DuckBehavior Controller => controller;
        public static DuckSystemBuckets Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }
    }
}