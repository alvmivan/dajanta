using ADTs;


namespace Ducks.Exposed
{
    public interface IDuck
    {
        ICurrency Price { get; }
        int ID { get; }
        IDuckType DuckType { get; }
        
    }
}