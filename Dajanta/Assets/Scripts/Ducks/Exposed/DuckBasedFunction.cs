namespace Ducks.Exposed
{
    public delegate void DuckBasedFunction(IDuck duck);
}