using UnityEngine;

namespace Ducks.Exposed
{
    public interface IDuckBehaviourApi
    {
        /// <summary>
        /// Notifies when the duck flies away
        /// </summary>
        event DuckBasedFunction OnDuckGoesAway;
        /// <summary>
        /// Notifies when the duck is completely submerged in the water
        /// </summary>
        event DuckBasedFunction OnDuckSinks;
        
        /// <summary>
        /// The Layer Mask for each duck's physics 
        /// </summary>
        LayerMask DucksLayerMask { get; }

        Vector3 DuckPosition { get; }


        void SpawnDuck();
        IDuck CurrentDuck();

        void KillDuck(IDuck duck);

        void FailShoot();


        void LetEscape();
    }
}