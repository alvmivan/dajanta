using Ducks.Internal.Behavior;

namespace Ducks.Exposed
{
    public static class DuckSystem
    {
        internal static DuckBehaviourApi Api { get; } = new DuckBehaviourApi();
        public static IDuckBehaviourApi API => Api;
    }
}