namespace ADTs
{
    public interface ICurrency
    {
        int Value { get; }
    }
}