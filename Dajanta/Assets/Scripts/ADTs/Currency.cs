using System;
using UnityEngine;

namespace ADTs
{
    [Serializable]
    internal struct Currency : ICurrency
    {
        [SerializeField] private int _value;
        public bool Equals(Currency other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            //Auto generated!!!!! :C
            if (ReferenceEquals(null, obj)) return false;
            return obj is Currency other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value;
        }

        public int Value => _value;

        private Currency(int value)
        {
            _value = value;
        }

        public static Currency operator +(Currency a, Currency b)
        {
            return new Currency(a.Value + b.Value);
        }
        public static Currency operator -(Currency a, Currency b)
        {
            return new Currency(a.Value - b.Value);
        }

        public static bool operator <(Currency a, Currency b)
        {
            return a.Value < b.Value;
        }

        public static bool operator >(Currency a, Currency b)
        {
            return a.Value > b.Value;
        }
        public static bool operator <=(Currency a, Currency b)
        {
            return a.Value <= b.Value;
        }

        public static bool operator >=(Currency a, Currency b)
        {
            return a.Value >= b.Value;
        }
        
        public static bool operator ==(Currency a, Currency b)
        {
            return a.Value == b.Value;
        }

        public static bool operator !=(Currency a, Currency b)
        {
            return a.Value != b.Value;
        }
        
        public static Currency operator -(Currency a)
        {
            return new Currency(- a.Value);
        }

        public static Currency operator *(Currency m, int factor)
        {
            return new Currency(m.Value * factor);
        }
        
        public static Currency operator /(Currency m, int factor)
        {
            return new Currency(m.Value / factor);
        }
        
        public static implicit operator Currency(int value)
        {
            return new Currency(value);
        }

      
    }
}