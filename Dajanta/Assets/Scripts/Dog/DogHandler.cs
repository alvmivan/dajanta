using System.Collections.Generic;
using LovelyInterpolation;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Dog
{
    internal class DogHandler : AbstractDogHandler
    {

        [SerializeField] private TMP_Text TextDialog;
        [SerializeField]private LayerMask myLayerMask;
        
        private IAnimatorDogHandler _animatorDogHandler = new AnimatorDogHandler();
                
        
        // ---------- Unity Callbacks --------

        private void OnValidate()
        {
            if (!barkSource) barkSource = GetComponent<AudioSource>();
            if (!barkSource) return;
            barkSource.loop = false;
            barkSource.playOnAwake = false;
        }


        //------- Animator Callbacks ------

        private void _on_bark()
        {
            barkSource.clip = barks[Random.Range(0, barks.Length)];
            barkSource.Play();
        }
    
        
    
        // --------- Overrides ----------
        
        public override void Dialog(string dialog, float seconds)
        {
            TextDialog.enabled = true;
            TextDialog.text = dialog;
            LovelyExtensions.WaitAndDo(() => TextDialog.text = "",seconds);
        }

        public override void TripleBark()
        {
            _animatorDogHandler.TriBark(animator);
        }

        public override void SimpleBark()
        {
            _animatorDogHandler.Bark(animator);
        }

        private TailState _tailState = TailState.Idle;

        public override TailState Tail
        {
            get => _tailState;
            set
            {
                _tailState = value;
                HandleTail();    
            }
        }

        public override HeadDirection Head { get => _animatorDogHandler.GetHead(); set=>_animatorDogHandler.Head(value,animator); }
        
        //todo: implement all
        public override LayerMask Layer => myLayerMask;

        public override void Remove()
        {
            Destroy(dogTransform.gameObject);
        }

        public override void JumpTo(Vector3 target)
        {
            MoveTarget(target,2.5f);
            _animatorDogHandler.Jump(animator);
        }

        public override void Walk(bool walk)
        {
            _animatorDogHandler.Walk(animator,walk);
        }

        public override void Sniff(bool sniff)
        {
            _animatorDogHandler.Sniff(animator,sniff);
        }

        public override void MoveTarget(Vector3 target,float time =1)
        {
            //todo: change for a better way
            dogTransform.InterpolatePosition(target,time);
            var rot = dogTransform.rotation;
            dogTransform.forward = target - dogTransform.position;
            var desiredRot= dogTransform.rotation;
            dogTransform.rotation = rot;
            dogTransform.InterpolateRotation(desiredRot, 0.2f);
        }

        

        private void HandleTail()
        {
            _animatorDogHandler.HandleTail(animator,_tailState);
        }
    }

    internal class AnimatorDogHandler : IAnimatorDogHandler
    {
        private static readonly int Happy = Animator.StringToHash("Happy");
        private static readonly int Alert = Animator.StringToHash("Alert");
        private static readonly int SingleBark = Animator.StringToHash("SingleBark");
        private static readonly int TripleBark = Animator.StringToHash("TripleBark");
        private static readonly int HeadDirection = Animator.StringToHash("HeadDirection");
        private static readonly int JumpVar = Animator.StringToHash("Jump");
        private static readonly int WalkVar = Animator.StringToHash("Walk");
        private static readonly int SniffVar = Animator.StringToHash("Sniff");
        
        
        private readonly Dictionary<TailState,bool[]> _happyAlertValues = new Dictionary<TailState, bool[]>
        {
            [TailState.Idle] = new []{false,false},
            [TailState.Happy] = new []{true,false},
            [TailState.Alert] = new []{false,true}
        };

        private readonly Dictionary<HeadDirection, int> _headDirection = new Dictionary<HeadDirection, int>
        {
            [Dog.HeadDirection.Left] = -1,
            [Dog.HeadDirection.Right] = 1,
            [Dog.HeadDirection.Front] = 0,
        };

        private HeadDirection _head;


        public void HandleTail(Animator animator, TailState state)
        {
            var answers = _happyAlertValues[state];
            animator.SetBool(Happy, answers[0]);
            animator.SetBool(Alert, answers[1]);
        }

        public void Bark(Animator animator)
        {
            animator.SetTrigger(SingleBark);
        }

        public void TriBark(Animator animator)
        {
            animator.SetTrigger(TripleBark);
        }

        public void Head(HeadDirection direction, Animator animator)
        {
            _head = direction;
            animator.SetInteger(HeadDirection,_headDirection[direction]);
        }

        public void Jump(Animator animator)
        {
            animator.SetTrigger(JumpVar);
        }

        public void Walk(Animator animator, bool walk)
        {
            animator.SetBool(WalkVar,walk);
        }

        
        public void Sniff(Animator animator, bool sniff)
        {
            animator.SetBool(SniffVar,sniff);
        }

        public HeadDirection GetHead()
        {
            return _head;
        }
    }

    public enum HeadDirection
    {
        Left,Front,Right
    }
}