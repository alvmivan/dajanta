using UnityEngine;

namespace Dog
{
    internal interface IAnimatorDogHandler
    {
        void HandleTail(Animator animator, TailState state);
        void Bark(Animator animator);
        void TriBark(Animator animator);
        void Head(HeadDirection direction,Animator animator);
        void Jump(Animator animator);
        void Walk(Animator animator, bool walk);

        void Sniff(Animator animator, bool sniff);
        HeadDirection GetHead();
    }
}