﻿using UnityEngine;

namespace Dog
{
    public abstract class AbstractDogHandler : MonoBehaviour
    {
        [SerializeField] protected Animator animator;
        [SerializeField] protected Transform dogTransform;

        [SerializeField] protected AudioSource barkSource;
        [SerializeField] protected AudioClip[] barks;

        public abstract void TripleBark();
        public abstract void SimpleBark();
        public abstract TailState Tail { get; set; }
        public abstract HeadDirection Head { get; set; }
        public abstract LayerMask Layer { get; }

        public abstract void Remove();
        public abstract void JumpTo(Vector3 target);
        public abstract void Walk(bool p0);
        public abstract void Sniff(bool p0);
        public abstract void MoveTarget(Vector3 target, float time=1);

        public abstract void Dialog(string dialog, float seconds);

    }

    public enum TailState
    {
        Idle,
        Alert,
        Happy,
    }
}