﻿using UnityEngine;

public class TesterMorir : MonoBehaviour//todo: borrar tester
{
    private Animator _animator;
    private static readonly int Alive = Animator.StringToHash("Alive");

// Start is called before the first frame update
    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            _animator.SetBool(Alive, false);
        }
    }
}
