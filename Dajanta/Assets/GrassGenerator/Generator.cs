﻿using System.Collections.Generic;
using UnityEngine;

namespace GrassGenerator
{
    public class Generator : MonoBehaviour
    {

        [SerializeField] private int grassQuantity=1;
        [SerializeField] private Vector2 planeSize=Vector2.one;
        [SerializeField] private GameObject[] grassModels; 
        
        
        
        //relative plane representation
        //[Tooltip("Relative to transform position")]
        private readonly ICollection<Vector3> _points = new LinkedList<Vector3>();
        
        
        
#if UNITY_EDITOR

        [Space(10)]
        [Header("Editor Stuff")]
        
        [SerializeField] private float cubeSize = .1f;
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(0.42f, 0.92f, 0.48f, 0.3f);
            var center = transform.position;
            foreach(var point in _points)
            {
                var place = point + center;
                Gizmos.DrawCube(place,Vector3.one * cubeSize);
            }
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireCube(center,new Vector3(planeSize.x,1,planeSize.y));
        }
#endif

        private void OnValidate()
        {
            ValidateVariables();
            _points.Clear();
            for (var i = 0; i < grassQuantity; i++)
            {
                _points.Add(GetRandomPoint());
            }
        }

        private void ValidateVariables()
        {
            planeSize.x = Mathf.Max(1f, planeSize.x);
            planeSize.y = Mathf.Max(1f, planeSize.y);
            grassQuantity = Mathf.Max(1, grassQuantity);
        }

        private void Start()
        {
            foreach (var point in _points)
            {
                InstantiateRandom(point).SetParent(transform);
            }
        }

        private Vector3 GetRandomPoint()
        {
            return new Vector3(Random.Range(-planeSize.x/2,planeSize.x/2),0,Random.Range(-planeSize.y/2,planeSize.y/2));
        }

        private Transform InstantiateRandom(Vector3 point)
        {
            var grass = Instantiate(grassModels[Random.Range(0, grassModels.Length)]);
            grass.transform.position = point + transform.position;
            return grass.transform;
        }
    }
}
