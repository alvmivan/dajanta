﻿using UnityEngine;

namespace Riggux
{
    public class RootBone : MonoBehaviour
    {
        [SerializeField] private BoneData data;

        private void OnValidate()
        {
            foreach (var child in GetComponentsInChildren<Transform>())
            {
                if (!child.GetComponent<Bone>())
                {
                    child.gameObject.AddComponent<Bone>();
                }

                child.GetComponent<Bone>().Data = data;
            }
        }
    }
}
