﻿using System;
using UnityEditor;
using UnityEngine;

namespace Riggux
{
    
    [ExecuteInEditMode]
    public class Bone : MonoBehaviour
    {
        #if UNITY_EDITOR
        //private Font myFont  = Font.CreateDynamicFontFromOSFont("Helvetica",20);
        
        private void OnDrawGizmosSelected()
        {
            var position = transform.position;
            Gizmos.color = Data.ArticulationColor;
            Gizmos.color = Color.white - Data.BoneColor;
            var color = Gizmos.color;
            color.a = 1;
            Gizmos.color = color;
            Gizmos.DrawWireSphere(position,Data.ArticulationRatio);
            
            var style = new GUIStyle();
            //style.font = myFont;
            style.focused = new GUIStyleState{textColor = Color.white};
            style.active = new GUIStyleState{textColor = new Color(0f, 0.38f, 0f)};
            style.normal = new GUIStyleState{textColor = Color.black};
            Handles.Label(position+Vector3.up*.04f,gameObject.name,style);
            
        }

        

        private void OnDrawGizmos()
        {
            var position = transform.position;
            Gizmos.color = Data.ArticulationColor;
            Gizmos.DrawWireSphere(position,Data.ArticulationRatio);
            Gizmos.color = Data.BoneColor;
            foreach (var bone in GetComponentsInChildren<Bone>())
            {
                if(bone.transform.parent == transform)
                BonesGizmos.DrawBone(position,bone.transform.position);
            }
            if(Data.ShowNames)
            Handles.Label(position+Vector3.up*.02f,gameObject.name);
        }

        private string lastName = "";
        private bool nameChanged = false;            
        private void OnValidate()
        {
            lastName = gameObject.name;
        }
        
        private void Update()
        {
            if (lastName != gameObject.name)
            {
                nameChanged = true;
                RenameChildren(gameObject.name,0);
            }
            lastName = gameObject.name;
        }

        private void RenameChildren(string myName, int index)
        {
            
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
            foreach (var bone in GetComponentsInChildren<Bone>())
            {
                if (bone.nameChanged) continue;
                
                bone.gameObject.name = $"{myName} {index}";
                bone.nameChanged = true;
                index++;
            } 
        }

        
        


#endif
        
        


        public BoneData Data { private get; set; }
    }
    
    [Serializable]
    public struct BoneData
    {
        public bool ShowNames;
        public Color BoneColor;
        public Color ArticulationColor;
        public float ArticulationRatio;
    }

    public static class BonesGizmos
    {
        public static void DrawBone(Vector3 from, Vector3 to)
        {
            var middlePoint = Vector3.Lerp(from, to, 1/3f);

            var direction = (to - from).normalized;
            var ortogonal1 =
                Vector3.Cross(direction, (direction != Vector3.one) ? direction + Vector3.one : Vector3.up);

            var ortogonal2 = Vector3.Cross(direction, ortogonal1);
            
            ortogonal1.Normalize();
            ortogonal2.Normalize();
            
            var ratio = Vector3.Distance(from,to)/16;


            ortogonal1 *= ratio;
            ortogonal2 *= ratio;

            var ort3 = (ortogonal1 + ortogonal2).normalized * ratio;
            var ort4 = (ortogonal1 - ortogonal2).normalized * ratio;
            
            
            
            var extraPoints = new Vector3[8];
            extraPoints[0] = middlePoint + ortogonal1;
            extraPoints[1] = middlePoint + ortogonal2;
            extraPoints[2] = middlePoint - ortogonal1;
            extraPoints[3] = middlePoint - ortogonal2;
            
            extraPoints[0+4] = middlePoint + ort3;
            extraPoints[1+4] = middlePoint + ort4;
            extraPoints[2+4] = middlePoint - ort3;
            extraPoints[3+4] = middlePoint - ort4;


            var color = Gizmos.color;
            
            Gizmos.color = new Color(0.3f,0.3f,0.3f,0.4f);
            Gizmos.DrawCube(middlePoint,Vector3.one*.01f);

            Gizmos.color = color;
            Gizmos.DrawLine(from,to);
            
            Gizmos.color -= new Color(.3f,.3f,.3f,.5f);
            
            for (var i = 0; i < 3; i++)
            {
                Gizmos.DrawLine(extraPoints[i],extraPoints[i+1]);
            }
            
            Gizmos.DrawLine(extraPoints[3],extraPoints[0]);
            
            
            for (var i = 0; i < 4; i++)
            {
            
                Gizmos.DrawLine(extraPoints[i],to);
                
                Gizmos.DrawLine(extraPoints[i],from);
            }
            
            
        }
    }
    
    
}
